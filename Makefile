all:
	@echo "Voilà"

upload:
	ssh isengard mkdir -p war-and-trash
	scp war.rkt trash.rkt SIM warntrash-student.tar.bz2 davidflorness@isengard:war-and-trash

archive:
	git archive -o davidflorness.tar.gz master SIM war.rkt trash.rkt doc/README Makefile
